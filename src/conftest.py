from datetime import datetime

import pytest
from app import create_app
from app import db as _db
from models import Client, ClientParking, Parking


@pytest.fixture
def app():
    app = create_app()
    app.config["TESTING"] = True
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///:memory:"
    with app.app_context():
        _db.create_all()
        client = Client(
            name="Test",
            surname="User",
            credit_card="1234567890123456",
            car_number="ABC123",
        )
        parking = Parking(
            address="Test Parking",
            opened=True,
            count_places=10,
            count_available_places=10,
        )
        client_parking_log = ClientParking(
            client=client, parking=parking, time_in=datetime.utcnow(), time_out=None
        )

        _db.session.add_all([client, parking, client_parking_log])
        _db.session.commit()

    yield app


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def db(app):
    return _db
