import factory
from faker import Faker
from models import Client, Parking

fake = Faker()


class ClientFactory(factory.Factory):
    class Meta:
        model = Client

    name = factory.Faker("first_name")
    surname = factory.Faker("last_name")
    credit_card = factory.LazyAttribute(
        lambda x: fake.credit_card_number() if fake.boolean() else None
    )
    car_number = factory.Faker("text", max_nb_chars=10)


class ParkingFactory(factory.Factory):
    class Meta:
        model = Parking

    address = factory.Faker("address")
    opened = factory.Faker("boolean")
    count_places = factory.Faker("random_int", min=1, max=100)
    count_available_places = factory.LazyAttribute(lambda x: x.count_places)


def test_create_client_with_factory(app, db):
    with app.app_context():
        client = ClientFactory()
        db.session.add(client)
        db.session.commit()
        assert client.id is not None


def test_create_parking_with_factory(app, db):
    with app.app_context():
        parking = ParkingFactory()
        db.session.add(parking)
        db.session.commit()
        assert parking.id is not None


def test_create_client_with_factorys(client, db):
    client_data = ClientFactory.build().__dict__
    client_data.pop("_sa_instance_state", None)
    response = client.post("/clients", json=client_data)
    assert response.status_code == 201
