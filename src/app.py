from datetime import datetime

from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def create_app():
    app = Flask(__name__)
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///prod.db"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    db.init_app(app)

    from models import Client, ClientParking, Parking

    @app.before_request
    def before_request_func():
        db.create_all()

    @app.route("/clients", methods=["GET", "POST"])
    def get_clients_handler():
        if request.method == "GET":
            clients = Client.query.all()
            clients_list = [client.to_json() for client in clients]
            return jsonify(clients_list)

        else:
            data = request.json
            new_client = Client(
                name=data["name"],
                surname=data["surname"],
                credit_card=data["credit_card"],
                car_number=data["car_number"],
            )
            if new_client:
                db.session.add(new_client)
                db.session.commit()
                return "", 201
            else:
                jsonify({"error": "bad data"})

    @app.route("/clients/<int:client_id>", methods=["GET"])
    def get_client_handler(client_id):
        client = Client.query.get(client_id)
        if client:
            return jsonify(client.to_json())
        else:
            return jsonify({"error": "Client not found"}), 404

    @app.route("/parkings", methods=["GET", "POST"])
    def create_parking_handler():
        if request.method == "POST":
            data = request.json
            new_parking = Parking(
                address=data["address"],
                opened=data["opened"],
                count_places=data["count_places"],
                count_available_places=data["count_available_places"],
            )
            db.session.add(new_parking)
            db.session.commit()
            return "", 201
        elif request.method == "GET":
            parkings = Parking.query.all()
            parking_list = [client.to_json() for client in parkings]
            return jsonify(parking_list)

    @app.route("/client_parkings", methods=["GET", "POST", "DELETE"])
    def client_parkings_handler():
        if request.method == "POST":
            data = request.json
            client_id = data["client_id"]
            parking_id = data["parking_id"]

            parking = Parking.query.get(parking_id)
            if parking.opened and parking.count_available_places > 0:
                parking.count_available_places -= 1
                new_log = ClientParking(client_id=client_id, parking_id=parking_id)
                db.session.add(new_log)
                db.session.commit()
                return "", 201
            else:
                return jsonify({"error": "Parking is not available"}), 400
        elif request.method == "GET":
            return jsonify({"error": "you need a POST not GET"}), 200

        elif request.method == "DELETE":
            data = request.json
            client_id = data["client_id"]
            parking_id = data["parking_id"]
            log = ClientParking.query.filter_by(
                client_id=client_id, parking_id=parking_id, time_out=None
            ).first()
            if log:
                parking = Parking.query.get(parking_id)
                parking.count_available_places += 1
                log.time_out = datetime.utcnow()
                db.session.commit()
                return "", 200
            else:
                return (
                    jsonify(
                        {"error": "Client is not checked in or has already checked out"}
                    ),
                    400,
                )
        else:
            return jsonify({"error": "METHOD NOT ALLOWED"}), 200

    return app
