import pytest


@pytest.mark.parametrize("endpoint", ["/clients", "/parkings", "/client_parkings"])
def test_get_endpoints_return_200(client, endpoint):
    response = client.get(endpoint)
    assert response.status_code == 200


def test_create_client(client):
    data = {
        "name": "New",
        "surname": "User",
        "credit_card": "1234567890123456",
        "car_number": "XYZ789",
    }
    response = client.post("/clients", json=data)
    assert response.status_code == 201


def test_create_parking(client):
    data = {
        "address": "New Parking",
        "opened": True,
        "count_places": 20,
        "count_available_places": 20,
    }
    response = client.post("/parkings", json=data)
    assert response.status_code == 201


@pytest.mark.parking
def test_check_in_parking(client):
    data = {"client_id": 1, "parking_id": 1}
    response = client.post("/client_parkings", json=data)
    assert response.status_code == 201


@pytest.mark.parking
def test_check_out_parking(client):
    data = {"client_id": 1, "parking_id": 1}
    response = client.delete("/client_parkings", json=data)
    assert response.status_code == 200
